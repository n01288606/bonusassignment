﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bonusAssignprob1.aspx.cs" Inherits="bonusAssignmentprob1.bonusAssignprob1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="blankDiv" runat="server">
        </div>
        <label>X:</label>
        <asp:TextBox runat="server" ID="myInteX"></asp:TextBox>
        <asp:CustomValidator ID="validateX" runat="server" ControlToValidate="myInteX" OnServerValidate="validateX_ServerValidate" Type="Integer"  ErrorMessage="Enter a valid non-zero number "></asp:CustomValidator> 
        <label>Y:</label>
        <asp:TextBox runat="server" ID="myInteY"></asp:TextBox>
          <asp:CustomValidator ID="validateY" runat="server" ControlToValidate="myInteY" OnServerValidate="validateY_ServerValidate" Type="Integer"  ErrorMessage="Enter a valid non-zero number "></asp:CustomValidator> 
         <asp:Button runat="server" ID="myButton" OnClick="myButton_Click" Text="Submit" />
    </form>
</body>
</html>
