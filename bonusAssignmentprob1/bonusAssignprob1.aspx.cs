﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignmentprob1
{
    public partial class bonusAssignprob1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*We determine a point on this quadrant by a(x, y) value, where x and y are both integers.A
            positive x value means that the point will fall to the right of the y-axis(vertical line).A positive y
            value means that the point will fall above the x-axis(horizontal line).A value of(0,0) means that
            the point will fall on none of the quadrants.
            EG. (4, -12) falls in quadrant 4. (-3, -1) falls in quadrant 3.
            Given input of two integers, create a server click function which prints a message to a blank div
            element of which quadrant the co - ordinate would fall. Validate your inputs to assume that x and
            y are non - zero(for the sake of simplicity).*/



        }

        protected void myButton_Click(object sender, EventArgs e)
        {
            //Quadrant 1 - x > 0 ----- y > 0
            //Quadrant 2 - x < 0 ------ y > 0
            //Quadrant 3 - x < 0 ------ y < 0
            //Quadrant 4 - x > 0 ----- y < 0

            // capture value x from text
            // capture value y  from text box
            // Display whether the page passed validation.
            if (Page.IsValid)
            {

                int x = int.Parse(myInteX.Text);
                int y = int.Parse(myInteY.Text);

                if (x > 0 && y > 0)
                {
                    blankDiv.InnerHtml = "quadrant 1";
                }

                else if (x < 0 && y > 0)
                {
                    blankDiv.InnerHtml = "quadrant 2";

                }
                else if (x < 0 && y < 0)
                {
                    blankDiv.InnerHtml = "quadrant 3";
                }
                else
                {
                    blankDiv.InnerHtml = "quadrant 4";
                }
            }           
        }

        public bool ValidateXnY(int value)
        {
            if (value == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void validateX_ServerValidate(object source, ServerValidateEventArgs args)
        {

            int i = int.Parse(args.Value);
            args.IsValid = ValidateXnY(i);
        }

        protected void validateY_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int i = int.Parse(args.Value);
            args.IsValid = ValidateXnY(i);
        }
    }
}