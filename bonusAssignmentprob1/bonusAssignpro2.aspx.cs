﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonusAssignmentprob1
{
    public partial class bonusAssignpro2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*An integer n is considered divisible if integer p can represent another integer k where the
equation is p*k = n. An example is that 12 is divisible by 4 because 3*4 = 12.

A prime number is divisible by only 1 and itself. A non-prime number can be divisible by a
number which is between 1 and itself. An example is that 13 is only divisible by 1 (13*1 = 13)
and 13 (1*13 = 13).

The modulus operator is useful for determining divisibility in C#. The modulus operator takes
two integers and returns the remainder when divided. (eg. 12%3 = 0, 14%3 = 2, 15%3 = 0, 3%3
= 0, 4%5 = 4).

Given a positive integer input, write a server click function which prints a message to a blank
div. The message will determine if a number is prime or not

(hint): How do you find if 107 is prime? Try 107%106, 107%105… etc. If you reach 1 before you
get a zero result, that means the number is prime.*/

        }
        protected void myButton_Click(object sender, EventArgs e)
        { 
            
            //not prime if 1%2= 0 
            // prime if 1%2= anything else?

            //capture text from first
           
            //display prime or not prime

            int priInt = int.Parse(myInt.Text);
            bool Flag = true;


            for (int i = priInt - 1; i < priInt; i--)
            {   

                if (priInt % i == 0)
                {
                    Flag = false;
                }

                if (Flag == false) { 

               blankDiv.InnerHtml = "Not Prime";
                 }
                else
                {
                    blankDiv.InnerHtml = "Prime";
                }
            }
              
        }
    }
}